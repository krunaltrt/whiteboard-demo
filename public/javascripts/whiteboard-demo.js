var socket = io.connect('http://localhost:3002');
var canvas = new fabric.Canvas('canvas');
var rect = new Rectangle(canvas);
rect.socket = socket;

socket.on('connect', function() {
     console.log('Client connected');
     socket.on('changeCanvas', function (data) {
       var jsonObj = data.canvasJsonObject;
       canvas.loadFromJSON(jsonObj.canvas);
       canvas.renderAll();
       canvas.setWidth(jsonObj.width);
       canvas.setHeight(jsonObj.height);
     });
})
